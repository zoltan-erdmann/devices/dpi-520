﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Text;
using System.IO.Ports;
using System.Threading;
using FFW.NSDevices;

namespace FFW.Devices.NSDPI520 {

	public class DPI520Addressed : DPI520ExtendedAddressed, IDisposable {

		public event PressureInRange PressureInRangeEvent;
		public event PressureChanged PressureChangedEvent;
		public event ErrorOccured ErrorOccuredEvent;

		private bool _isOpened;
		private const int LoopID = 0;
		private string _receivedLoopMessage;
		private bool inRangeTriggered = false;
		private float _currentPressure;
		private DateTime _latestWrongPressureRead;
		private float _setPointPressure;
		private object _statusLock = new object();
		private object _eventLock = new object();
		private int _address;

		//private AutoResetEvent _functionSync = new AutoResetEvent(false);
		private AutoResetEvent _loopFunctionSync = new AutoResetEvent(false);
		private AutoResetEvent _loopFunctionHandledSync = new AutoResetEvent(true);
		private volatile bool _loop = false;
		protected Thread _listenerThread = null;
		

		#region Event triggers

		protected void TriggerPressureInRange(PressureArgs args) {
			if(PressureInRangeEvent != null) {
				PressureInRangeEvent(this, args);
			}
		}

		protected void TriggerPressureChanged(PressureArgs args) {
			if(PressureChangedEvent != null) {
				PressureChangedEvent(this, args);
			}
		}

		protected void TriggerErrorOccured(ushort error) {
			if(ErrorOccuredEvent != null) {
				ErrorOccuredEvent(this, error);
			}
		}

		#endregion

		private void StopThread() {
			_loop = false;
			if(_listenerThread == null) {
				return;
			}
			_listenerThread.Join();
			//if(_listenerThread.ThreadState == ThreadState.Running) {
			//    _listenerThread.Abort();
			//}
			_listenerThread = null;
		}

		protected override void OnTreadedMessageReceivedEvent(IRS232Device sender, ThreadedMessage message) {
			//lock(_lockObject) {
				base.OnTreadedMessageReceivedEvent(sender, message);
				if(message.ID == LoopID) {
					_loopFunctionHandledSync.WaitOne(5000);
					_receivedLoopMessage = string.Copy(message.Message);
					_loopFunctionSync.Set();
					//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Error, "Loop Set"));
				}
			//}
		}

		protected void GetEventStatusInformation(int address, int id) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 3)));
			string message = string.Format("#?{0}", address.ToString("D2"));
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(LoopID, sb.ToString()));
		}

		protected void GetCurrentStatus(int address, int id) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 6)));
			string message = string.Format("#?{0}", address.ToString("D2"));
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(LoopID, sb.ToString()));
		}

		protected void SendNewLine(int id) {
			WriteRawMessage(new ThreadedMessage(id, Terminator.ToString()));
		}

		protected void InformationRefresher(object o) {
			int address = (int)o;
			bool result;
			_latestWrongPressureRead = DateTime.Now;

			CurrentStatus currentStatus = null;
			while(_loop) {
				try {
					//lock(_lockObject) {
						GetCurrentStatus(address, LoopID);

						result = _loopFunctionSync.WaitOne(WaitPeriod);
						currentStatus = ParseCurrentStatus(_receivedLoopMessage);

						if(currentStatus != null) {
							if(_currentPressure != currentStatus.Pressure) {
								_currentPressure = currentStatus.Pressure;
								TriggerPressureChanged(new PressureArgs(currentStatus.Pressure, currentStatus.Unit));
							}
							if(currentStatus.ErrorCode != 0) {
								TriggerErrorOccured(currentStatus.ErrorCode);
							}
						}
						if(currentStatus.Pressure != _setPointPressure) {
							_latestWrongPressureRead = DateTime.Now;
						} else if(!inRangeTriggered && DateTime.Now.Subtract(_latestWrongPressureRead).Ticks > (new TimeSpan(0, 0, 45)).Ticks) {
							TriggerPressureInRange(new PressureArgs(currentStatus.Pressure, currentStatus.Unit));
							inRangeTriggered = true;
						}
					//}
						if(!_loop) {
							break;
						}
					Thread.Sleep(1000);
					if(!_loop) {
						break;
					}
				} catch(Exception) {
					continue;
				} finally {
					_loopFunctionHandledSync.Set();
				}
			}
		}

		public DPI520Addressed(string portName) : base(portName) {
		}


		public DPI520Addressed(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, Handshake handshake)
			: base(portName, baudRate, parity, dataBits, stopBits, handshake) {
		}

		public void AttachListenerhToDevice(int address) {
			if(_listenerThread != null) {
				throw new Exception("Allready listening to a device. Detach listerner first.");
			}
			_listenerThread = new Thread(InformationRefresher);
			_loop = true;
			_listenerThread.Start(address);
		}

		public void DetachListenerFromDevice() {
			StopThread();
		}

		public void Dispose() {
			Close();
		}

		~DPI520Addressed() {
			Close();
		}

		public override void Open() {
			//if(IsOpen) {
			//    Close();
			//}
			//Thread.Sleep(2000);

			base.Open();
			try {
				ClearDevice();
				ClearInterface();
				//ClearDevice();
			} catch(Exception ex) {
				throw new Exception("Invalid protocol exception (DPI520).\n"+ex.ToString());
			}
			int[] addresses = GetDevices();
			if(addresses.Length != 1) {
				throw new Exception("Invalid number of devices!");
			}
			_address = addresses[0];
			UnlistenAll();
			AddListener(_address);
			SetControlMode(ControlMode.Remote);
			AttachListenerhToDevice(_address);
			_isOpened = true;
		}

		public override void Close() {
			if(_isOpened) {
				try {
					DetachListenerFromDevice();
				} catch(Exception) { }
				try {
					SetControlMode(ControlMode.Local);
				} catch(Exception) { }
			}
			base.Close();
		}

		public override void SetPressure(float pressure, Scale scale, ushort wait = 20) {
			_latestWrongPressureRead = DateTime.Now;
			inRangeTriggered = false;
			_setPointPressure = pressure;
			base.SetPressure(pressure, scale, wait);
		}

		public override void SetPressure(float pressure, ScaleUnits unit = ScaleUnits.mbar, ushort wait = 20) {
			_latestWrongPressureRead = DateTime.Now;
			inRangeTriggered = false;
			_setPointPressure = pressure;
			base.SetPressure(pressure, unit, wait);
		}


		public DeviceInformation GetDeviceInformation() {
			return GetDeviceInformation(_address);
		}

		public InterfaceInformation GetInterfaceInformation() {
			return GetInterfaceInformation(_address);
		}

		public EventStatusInformation GetEventStatusInformation() {
			return GetEventStatusInformation(_address);
		}

		public SetupInformation GetSetupInformation() {
			return GetSetupInformation(_address);
		}

		public CurrentStatus GetCurrentStatus() {
			return GetCurrentStatus(_address);
		}

		public ControlMode GetControlMode() {
			return GetControlMode(_address);
		}

		public Scale GetScale() {
			return GetScale(_address);
		}

		public float Pressure { get { return _currentPressure; } }
	}
}
