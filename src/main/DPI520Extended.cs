﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Globalization;
using FFW.NSDevices;

namespace FFW.Devices.NSDPI520 {
	public class DPI520Extended : DPI520Base {

		protected const int FunctionID = 1;
		private AutoResetEvent _functionSync = new AutoResetEvent(false);
		private string _receivedMessage;
		protected object _lockObject = new object();

		protected virtual void OnTreadedMessageReceivedEvent(IRS232Device sender, ThreadedMessage message) {
			if(message.ID == FunctionID) {
				// Mivel nem érkezhet kérdés nélkül üzenet nem kerül Lock blokkba. Különben kéne.
				_receivedMessage = string.Copy(message.Message);
				_functionSync.Set();
			}
		}

		public DPI520Extended(string portName)
			: base(portName) {
			TreadedMessageReceivedEvent += new TreadedMessageReceived(OnTreadedMessageReceivedEvent);
		}


		public DPI520Extended(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, Handshake handshake)
			: base(portName, baudRate, parity, dataBits, stopBits, handshake) {
			TreadedMessageReceivedEvent += new TreadedMessageReceived(OnTreadedMessageReceivedEvent);
		}

		public DeviceInformation GetDeviceInformation() {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("N5{0}", Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Getting device information from device failed!");
			}
			int pos = _receivedMessage.IndexOf(Terminator);
			if(pos > 0) {
				_receivedMessage = _receivedMessage.Substring(pos).TrimStart();
			}
			string[] parts = _receivedMessage.Split(' ');
			if(parts.Length < 4) {
				throw new ArgumentException("Invalid message returned from device.");
			}
			pos = parts[3].IndexOf(':');
			if(pos > -1) {
				parts[3] = parts[3].Substring(pos + 1);
			}
			return new DeviceInformation(parts[0], parts[1], parts[2], parts[3].Trim('.'));
		}

		public InterfaceInformation GetInterfaceInformation() {
			string message = string.Format("{0}|{1}", ComposeMessage(new ControlCommand(ControlCommands.N, 2)), Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, message));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Getting interface information from device '{0}' failed!");
			}
			_receivedMessage = _receivedMessage.Trim();
			ControlMode mode = (ControlMode)UInt16.Parse(_receivedMessage.Substring(4, 1));
			Scale scale = (Scale)UInt16.Parse(_receivedMessage.Substring(6, 1));
			DataSources source = (DataSources)UInt16.Parse(_receivedMessage.Substring(8, 1));
			bool controllerOpen = _receivedMessage.Substring(10, 1).Equals("1");
			Interrupts interrupt = (Interrupts)UInt16.Parse(_receivedMessage.Substring(12, 1));
			bool isolationValve = _receivedMessage.Substring(15, 1).Equals("1");

			return new InterfaceInformation(mode, scale, source, controllerOpen, interrupt, isolationValve);
		}

		public EventStatusInformation GetEventStatusInformation() {
			string message = string.Format("{0}|{1}", ComposeMessage(new ControlCommand(ControlCommands.N, 3)), Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, message));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Getting event status information from device '{0}' failed!");
			}
			return ParseEventStatusInformation(_receivedMessage);
		}

		protected EventStatusInformation ParseEventStatusInformation(string message) {
			int pos = message.IndexOf(Terminator);
			if(pos > 0) {
				message = message.Substring(pos).TrimStart();
			}
			ushort inRange = UInt16.Parse(message.Substring(0, 1));
			ushort error = 0;
			int errorPos = message.IndexOf('@');
			if(errorPos > -1) {
				error = UInt16.Parse(message.Substring(errorPos + 1, 2), NumberStyles.HexNumber);
			}
			return new EventStatusInformation(inRange, error);
		}

		public SetupInformation GetSetupInformation() {
			string message = string.Format("{0}|{1}", ComposeMessage(new ControlCommand(ControlCommands.N, 8)), Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, message));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Getting setup information from device failed!");
			}
			CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
			ci.NumberFormat.CurrencyDecimalSeparator = ".";

			_receivedMessage = _receivedMessage.Trim();
			ControlMode mode = (ControlMode)UInt16.Parse(_receivedMessage.Substring(4, 1));
			Scale scale = (Scale)UInt16.Parse(_receivedMessage.Substring(6, 1));
			DataSources source = (DataSources)UInt16.Parse(_receivedMessage.Substring(8, 1));
			bool controllerOpen = _receivedMessage.Substring(10, 1).Equals("1");
			Interrupts interrupt = (Interrupts)UInt16.Parse(_receivedMessage.Substring(12, 1));

			ushort notationCode = UInt16.Parse(_receivedMessage.Substring(14, 1));
			uint waitTime = UInt32.Parse(_receivedMessage.Substring(16, 3));
			ushort errorStatus = UInt16.Parse(_receivedMessage.Substring(20, 1), NumberStyles.HexNumber);
			ushort terminatorCode = UInt16.Parse(_receivedMessage.Substring(22, 1));
			ushort rate = UInt16.Parse(_receivedMessage.Substring(24, 1));
			float variableRate = float.Parse(_receivedMessage.Substring(26, 7), NumberStyles.Any, ci);
			//TODO:!!!! Parsolni, vagy elhagyni!
			//ScaleUnits unit = (ScaleUnits)UInt16.Parse(_receivedMessage.Substring(34, 6));
			bool isTareOn = _receivedMessage.Substring(41, 1).Equals("1");// bool.Parse(_receivedMessage.Substring(41, 1));
			float tareValue = float.Parse(_receivedMessage.Substring(43, 7), NumberStyles.Any, ci);

			return new SetupInformation(mode, scale, source, controllerOpen, interrupt, notationCode,
				waitTime, errorStatus, terminatorCode, rate, variableRate, /*ScaleUnits.mbar,*/ isTareOn, tareValue);
		}

		public CurrentStatus GetCurrentStatus() {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}",
				ComposeMessage(new ControlCommand(ControlCommands.N, 6)), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Getting current status information from device '{0}' failed!");
			}
			return ParseCurrentStatus(_receivedMessage);
		}

		protected CurrentStatus ParseCurrentStatus(string message) {
			ushort error = 0;
			int errorPos = message.IndexOf('@');
			if(errorPos > -1) {
				error = UInt16.Parse(message.Substring(errorPos + 1, 2), NumberStyles.HexNumber);
				message = message.Substring(errorPos + 3);
			}
			ScaleUnits unit = (ScaleUnits)UInt16.Parse(message.Substring(message.Length - 2, 2));
			message = message.Substring(0, message.Length - 3);
			DataSources source = (DataSources)UInt16.Parse(message.Substring(message.Length - 1, 1));
			message = message.Substring(0, message.Length - 2);
			Scale scale = (Scale)UInt16.Parse(message.Substring(message.Length - 1, 1));
			message = message.Substring(0, message.Length - 2);
			ControlMode mode = (ControlMode)UInt16.Parse(message.Substring(message.Length - 1, 1));
			message = message.Substring(0, message.Length - 2);
			message = message.Substring(0, message.Length - 3);
			CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
			ci.NumberFormat.CurrencyDecimalSeparator = ".";
			float pressure = float.Parse(message, NumberStyles.Any, ci);

			return new CurrentStatus(pressure, mode, scale, source, unit, error);
		}

		public virtual void SetPressure(float pressure, Scale scale, ushort wait = 20) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.S, (ushort)scale)));
			sb.Append(Delimiter);
			//sb.Append(ComposeMessage(new ControlCommand(ControlCommands.I, 3)));
			//sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.P, pressure, ControlParameterType.ControlValue)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.W, wait)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.C, 1)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));

			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Setting set point pressure to '{0}' failed!", pressure));
			}
		}

		public virtual void SetPressure(float pressure, ScaleUnits unit = ScaleUnits.mbar, ushort wait = 20) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.S, 3)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.U, (int)unit)));
			sb.Append(Delimiter);
			//sb.Append(ComposeMessage(new ControlCommand(ControlCommands.I, 3)));
			//sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.P, pressure, ControlParameterType.ControlValue)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.W, wait)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.C, 1)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Setting set point pressure to '{0}{1}' failed!", pressure, unit));
			}
		}

		public ControlMode GetControlMode() {
			CurrentStatus status = GetCurrentStatus();
			return status.ControlMode;
		}

		public void SetControlMode(ControlMode controlMode) {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("R{0} N6{1}", ((int)controlMode), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Setting control mode to '{0}' failed!", controlMode));
			}
		}

		public Scale GetScale() {
			return GetCurrentStatus().Scale;
		}

		public void SetScale(Scale scale) {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}",
				ComposeMessage(new ControlCommand(ControlCommands.S, (ushort)scale)), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Setting scale to '{0}' failed!", scale));
			}
		}

		public void OpenIsolationValve() {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}", ComposeMessage(new ControlCommand(ControlCommands.E, 1)), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Opening isolation valve failed!");
			}
		}

		public void CloseIsolationValve() {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}", ComposeMessage(new ControlCommand(ControlCommands.E, 0)), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Closing isolation valve failed!");
			}
		}
	}
}
