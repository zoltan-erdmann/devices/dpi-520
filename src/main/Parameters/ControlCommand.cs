﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;

namespace FFW.Devices.NSDPI520 {

	public enum ControlParameterType { ControlSelection, ControlValue }

	public class ControlCommand {

		public ControlCommands ControlCode { get; set; }
		public int ControlSelection { get; set; }
		public float ControlValue { get; set; }

		public ControlCommand() {
			ControlSelection = -1;
			ControlValue = float.MinValue;
		}

		public ControlCommand(ControlCommands controlCode) : this() {
			ControlCode = controlCode;
		}

		public ControlCommand(ControlCommands controlCode, int controlSelection) : this(controlCode) {
			ControlSelection = controlSelection;
		}

		public ControlCommand(ControlCommands controlCode, int controlSelection, float controlValue)
			: this(controlCode, controlSelection) {
			ControlValue = controlValue;
		}

		public ControlCommand(ControlCommands controlCode, float controlParam, ControlParameterType type)
			: this(controlCode) {
			if(type == ControlParameterType.ControlValue) {
				ControlValue = controlParam;
			} else {
				ControlSelection = Convert.ToInt32(controlParam);
			}
		}
	}
}
