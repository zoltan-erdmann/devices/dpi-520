﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public class DeviceInformation {

		public string Name { get; private set; }
		public string Accuracy { get; private set; }
		public string FullScale { get; private set; }
		public string SerialNumber { get; private set; }

		public DeviceInformation(string name, string accuracy, string fullScale, string serialNumber) {
			Name = name;
			Accuracy = accuracy;
			FullScale = fullScale;
			SerialNumber = serialNumber;
		}
	}
}
