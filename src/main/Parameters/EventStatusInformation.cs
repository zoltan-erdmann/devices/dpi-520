﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public class EventStatusInformation {

		public ushort InLimitStatus { get; protected set; }
		public ushort ErrorCode { get; protected set; }

		public EventStatusInformation(ushort inLimitStatus, ushort errorCode) {
			InLimitStatus = inLimitStatus;
			ErrorCode = errorCode;
		}
	}
}
