﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public class PressureArgs {

		public float Pressure { get; protected set; }
		public ScaleUnits Unit { get; protected set; }

		public PressureArgs(float pressure, ScaleUnits unit) {
			Pressure = pressure;
			Unit = unit;
		}

	}
}
