﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public class SetupInformation {

		public ControlMode ControlMode { get; protected set; }
		public Scale Scale { get; protected set; }
		public DataSources DataSource { get; protected set; }
		public bool IsControllerEnabled { get; set; }
		public Interrupts Interrupt { get; protected set; }
		public ushort NotationCode { get; protected set; }
		public uint WaitTime { get; set; }
		public ushort ErrorStatus { get; protected set; }
		public ushort TerminatorCode { get; protected set; }
		public ushort Rate { get; set; }
		public float VariableRate { get; protected set; }
		//public ScaleUnits Unit { get; protected set; }
		public bool IsTareOn { get; protected set; }
		public float TareValue { get; protected set; }

		public SetupInformation(ControlMode controlMode, Scale scale, DataSources dataSource,
			bool isControllerEnabled, Interrupts interrupt, ushort notationCode, uint waitTime, ushort errorStatus,
			ushort terminatorCode, ushort rate, float variableRate, /*ScaleUnits unit,*/ bool isTareOn, float tareValue) {
				ControlMode = controlMode;
				Scale = scale;
				DataSource = dataSource;
				IsControllerEnabled = IsControllerEnabled;
				Interrupt = interrupt;
				NotationCode = notationCode;
				WaitTime = waitTime;
				ErrorStatus = errorStatus;
				TerminatorCode = terminatorCode;
				Rate = rate;
				VariableRate = variableRate;
				//Unit = unit;
				IsTareOn = isTareOn;
				TareValue = tareValue;
		}

	}
}
