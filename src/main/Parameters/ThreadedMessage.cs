﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public class ThreadedMessage {

		public int ID { get; set; }
		public string Message { get; set; }
		DeviceThreadedNotification Listener { get; set; }
		
		public ThreadedMessage(int id, string message) {
			ID = id;
			Message = message;
		}

	}
}
