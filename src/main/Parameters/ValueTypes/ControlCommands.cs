﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public enum ControlCommands {

		/// <summary>
		/// Set to local mode - 'M'.
		/// This command places the instrument into local mode.
		/// </summary>
		M,

		/// <summary>
		/// Set mode - 'R'.
		/// This command places the instrument into either local or remote mode and turns the controller off.
		/// Value 0 sets the instrument to local and value = 1, to remote mode.
		/// </summary>
		R,

		/// <summary>
		/// Set the scale units - 'S'.
		/// This command selects the pressure units. It takes the value 0 to 3.
		/// S0 to S2 select the units set-up for the function keys F1 to F3.
		/// S3 selects the units configured by the U command.
		/// </summary>
		S,
		
		/// <summary>
		/// Units - 'U'.
		/// Used in conjunction with the S3 command, this command allows any of the instrument's units to be selected.
		/// The command taked a value which defines the required unit.
		/// </summary>
		U,

		/// <summary>
		/// Data select - 'D'.
		/// The D command determines the source of data returned following a data request for notations N0 and N1.
		/// It takes the value 0 to 2.
		/// </summary>
		D,

		/// <summary>
		/// Output data format - 'N'.
		/// Notation code selection. selects the format of the data when data is requested from the instrument.
		/// The command taked the value 0 to 8 according to the required format.
		/// </summary>
		N,

		/// <summary>
		/// Iterrupt - 'I'.
		/// The interrupt command sets the instrument to return a message when a specified event has occured.
		/// It takes the value 0 to 7 to determine the event as follows.
		/// 0	No interrupt
		/// 1	Interrupt on "Error".
		/// 2	Interrupt on "In limit".
		/// 3	Interrupt on "In limit" and "Error".
		/// 4	Interrupt on "End of conversation".
		/// 5	Interrupt on "Error" and "End of conversation".
		/// 6	Interrupt on "In limit" and "End of conversation".
		/// 7	Interrupt on "In limit", "Error" and end of conversation.
		/// 
		/// Error:
		/// This occures when a command's syntax is not understood or the command's parameters are out of range.
		/// 
		/// In-limits:
		/// When a new set-point is entered the wait timer is reset (see W command).
		/// When the reading is within 0.05% FS of the set-point the wait timer is decremented once a second
		/// (if outside these limits then the timer is reset to it's original value).
		/// When the timer reaches zero then in-limits signal is generated.
		/// 
		/// End of conversation:
		/// Each time the instrument has a new reading (approximately 0.7 second) this event is generated.
		/// </summary>
		I,

		/// <summary>
		/// Wait - 'W'.
		/// Selects the wait time before the in-limits signal is active. It takes a value which is the wait time in seconds (0 to 100).
		/// </summary>
		W,

		/// <summary>
		/// Controller On/Off - 'C'.
		/// Turns the controller on/off. It takes the value 0 or 1 as follows.
		/// 0	controller off.
		/// 1	controller on.
		/// </summary>
		C,

		/// <summary>
		/// Pressure set-point - 'P'.
		/// This command allows a new pressure set-point to be entered. Its value is the new set-point in the current units.
		/// The control value is a number corresponding to the new set-point demand pressure in the current units.
		/// </summary>
		P,
		
		/// <summary>
		/// Ratio - '/'.
		/// This command sets one of the twelve preset division ratios. The current set-point is then divided by this ratio.
		/// The command takes the values 0 to 11.
		/// </summary>
		Ratio,

		/// <summary>
		/// Preset - '*'.
		/// This command selects one of the twelve preset values. It takes the value 0 to 11.
		/// </summary>
		Preset,

		/// <summary>
		/// Error reporting On/Off - '@'.
		/// This command turns error reporting on or off. It takes the value 0 for Off and 1 for On.
		/// When turned on, errors are reported as part of data output as defined in the "Output code format".
		/// </summary>
		ErrorReporting,

		/// <summary>
		/// Selects the rate of change of pressure used to achieve set-point. The rate is as programmed on the function keys.
		/// 0	rate programmed on key F1.
		/// 1	rate programmed on key F2.
		///	2	rate programmed on key F3.
		/// </summary>
		J,
		
		/// <summary>
		/// Zero instrument - 'O1'.
		/// This command initiates a manual zero on the currently selected pressure range.
		/// </summary>
		O1,

		/// <summary>
		/// Sets the rate to a value in current units per second. The rate is automatically set to varible rate.
		/// This value is not stored in non volatile memory.
		/// </summary>
		V,

		/// <summary>
		/// Isolation valve Open/Close - 'E'.
		/// Controls the state of the isolation valve. It takes the value 0 for closed and 1 for open.
		/// </summary>
		E,

		/// <summary>
		/// Open isolation valve - 'F'.
		/// This command has been implemented for compatibility with the druk DPI 510 pressure controller.
		/// It takes the value of either 20 (isolation valve closed) or 21 (isolation valve open).
		/// Functionally it controls the state of the isolation valve in the same way as the E command.
		/// </summary>
		F,

		/// <summary>
		/// Tare value - 'B'.
		/// This command sets a value to be tared from the measured pressure.
		/// It takes the value of the tare in current units.
		/// </summary>
		B,

		/// <summary>
		/// Tare On/Off - 'T'.
		/// This command initiates the subtraction of the tare value from the measured pressure.
		/// It takes the value 0 for Off and 1 for On.
		/// </summary>
		T
		
	}
}
