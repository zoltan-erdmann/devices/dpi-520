﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public enum Scale : ushort {
		PredefinedInF1 = 0,
		PredefinedInF2 = 1,
		PredefinedInF3 = 2,
		CustomScale = 3
	}
}
