﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public enum Interrupts : ushort {
		NoInterrupt = 0,
		InterruptOnError = 1,
		InterruptOnInLimit = 2,
		InterruptOnInLimitAndError = 3,
		InterruptOnEndOfConversation = 4,
		InterruptOnErrorAndEndOfConversation = 5,
		InterruptOnInLimitAndEndOfConversation = 6,
		InterruptOnInLimitAndErrorAndEndOfConversation = 7
	}
}
