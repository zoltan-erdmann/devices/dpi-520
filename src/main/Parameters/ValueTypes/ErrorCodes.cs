﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public enum ErrorCodes : ushort {
		NoError = 0,
		CommandNotAccepted = 1,
		SecondaryAddressNotAvailable = 2,
		DataStringNotValid = 4,
		ReadingInLimits = 8,
		OverRange = 16,
		EndOfConversation = 32,
		ValveOverTemperature = 64,
		ChecksumError = 128
	}
}
