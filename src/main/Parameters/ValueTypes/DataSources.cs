﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public enum DataSources : ushort {
		CurrentPressure = 0,
		SetPointPressure = 1,
		FrontPanelReading = 2
	}
}
