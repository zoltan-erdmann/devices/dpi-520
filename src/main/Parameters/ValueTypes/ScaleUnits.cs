﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public enum ScaleUnits {
		/// <summary>
		/// Pascal (Pa).
		/// </summary>
		Pa = 1,

		/// <summary>
		/// kilo-Pascal (kPa).
		/// </summary>
		kPa = 2,

		/// <summary>
		/// mega-Pascal (MPa).
		/// </summary>
		MPa = 3,

		/// <summary>
		/// milibar (mbar).
		/// </summary>
		mbar = 4,

		/// <summary>
		/// bar (bar).
		/// </summary>
		bar = 5,

		/// <summary>
		/// kilogram per square centimetre (kg/cm2).
		/// </summary>
		kbpcm2 = 6,

		/// <summary>
		/// kilogram per square metre (kg/m2).
		/// </summary>
		kgpm2 = 7,

		/// <summary>
		/// milimetre of mercury (mmHg).
		/// </summary>
		mmHG = 8,

		/// <summary>
		/// centimetre of mercury (cmHg).
		/// </summary>
		cmHG = 9,

		/// <summary>
		/// metre of mercury (mHg).
		/// </summary>
		mHG = 10,

		/// <summary>
		/// milimetre of water (mmH2o).
		/// </summary>
		mmH2O = 11,

		/// <summary>
		/// centimetre of water (cmH2o).
		/// </summary>
		cmH2O = 12,

		/// <summary>
		/// metre of water (mH2o).
		/// </summary>
		mH2O = 13,

		/// <summary>
		/// 1/760 x 1 atm (1 mm mercury) (torr).
		/// </summary>
		torr = 14,

		/// <summary>
		/// atmosphere (atm).
		/// </summary>
		atm = 15,

		/// <summary>
		/// pound per square inch (psi).
		/// </summary>
		psi = 16,

		/// <summary>
		/// pound force per square foot (lbf/ft2).
		/// </summary>
		lbfpft2 = 17,

		/// <summary>
		/// inch of mercury (inHg).
		/// </summary>
		inHg = 18,

		/// <summary>
		/// inch of water @ 4°C (''H2O04).
		/// </summary>
		inH2O04 = 19,

		/// <summary>
		/// feet of water @ 4°C ('H2O04).
		/// </summary>
		feetH2O04 = 20,

		/// <summary>
		/// Special unit (SPEC'L).
		/// </summary>
		SPECL = 21,

		/// <summary>
		/// inch of water @ 20°C (''H2O20).
		/// </summary>
		inchH2O20 = 22,

		/// <summary>
		/// feet of water @ 20°C ('H2O20).
		/// </summary>
		feetH2O20 = 23,

		/// <summary>
		/// hecta-Pascal (hPa).
		/// </summary>
		hPa = 24
	}
}
