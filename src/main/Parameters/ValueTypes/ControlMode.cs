﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public enum ControlMode : ushort {
		Local = 0,
		Remote = 1
	}
}
