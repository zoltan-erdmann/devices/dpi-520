﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public class InterfaceInformation {

		public ControlMode ControlMode { get; protected set; }
		public Scale Scale { get; protected set; }
		public DataSources DataSource { get; protected set; }
		public bool IsControllerEnabled { get; set; }
		public Interrupts Interrupt { get; protected set; }
		public bool IsIsolationValveOpen { get; protected set; }

		public InterfaceInformation(ControlMode controlMode, Scale scale, DataSources dataSource,
			bool isControllerEnabled, Interrupts interrupt, bool isIsolationValveOpen) {
				ControlMode = controlMode;
				Scale = scale;
				DataSource = dataSource;
				IsControllerEnabled = IsControllerEnabled;
				Interrupt = interrupt;
				IsIsolationValveOpen = isIsolationValveOpen;
		}
	}
}
