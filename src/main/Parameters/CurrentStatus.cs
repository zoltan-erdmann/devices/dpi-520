﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSDPI520 {
	public class CurrentStatus {

		public float Pressure { get; protected set; }
		public ControlMode ControlMode { get; protected set; }
		public Scale Scale { get; protected set; }
		public DataSources DataSource { get; protected set; }
		public ScaleUnits Unit { get; protected set; }
		public ushort ErrorCode { get; protected set; }

		public CurrentStatus(float pressure, ControlMode controlMode, Scale scale,
			DataSources dataSource, ScaleUnits unit, ushort errorCode) {
				Pressure = pressure;
				ControlMode = controlMode;
				Scale = scale;
				DataSource = dataSource;
				Unit = unit;
				ErrorCode = errorCode;
		}
	}
}
