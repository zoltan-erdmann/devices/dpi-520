﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
// SPDX-License-Identifier: LGPL-3.0-only

using System.Threading;
using System.Globalization;
using FFW.NSDevices;

namespace FFW.Devices.NSDPI520 {

	public class DPI520BaseAddressed : IRS232Device {

		public event DeviceThreadedNotification DeviceNotificationEvent;
		public event TreadedMessageReceived TreadedMessageReceivedEvent;

		private StringBuilder _receiveBuffer = new StringBuilder();
		private SerialPort _serialPort;
		private Queue<ThreadedMessage> _requestOrder = new Queue<ThreadedMessage>();
		private object _lockObject = new object();
		private AutoResetEvent _communicationSync = new AutoResetEvent(true);

		public char Terminator = '\r';
		public const char Delimiter = ' ';
		protected const char CheckSumDelimiter = '|';
		public int WaitPeriod = 5000;
		//public Dispatcher Dispatcher { get; protected set; }

		public string Name { get { return ConstantPropertiesResource.ProductNameAddressed; } }
		public string ManufacturerName { get { return ConstantPropertiesResource.ManufacturerNameAddressed; } }

		public string PortName { get { return _serialPort.PortName; } }
		public int BaudRate { get { return _serialPort.BaudRate; } }
		public Parity Parity { get { return _serialPort.Parity; } }
		public int DataBits { get { return _serialPort.DataBits; } }
		public StopBits StopBits { get { return _serialPort.StopBits; } }

		#region Event triggers

		protected void TriggerDeviceNotification(int address) {
			if(DeviceNotificationEvent != null) {
				DeviceNotificationEvent(this, address);
			}
		}

		protected void TriggerTreadedMessageReceived(ThreadedMessage message) {
			if(TreadedMessageReceivedEvent != null) {
				TreadedMessageReceivedEvent(this, message);
			}
		}

		#endregion

		protected string CheckSum(string message) {
			int sum = 0;
			foreach(char c in message) {
				sum += (int)c;
			}
			return (sum % 100).ToString("D2");
		}

		protected string ComposeMessage(ControlCommand command) {
			StringBuilder sb = new StringBuilder();
			sb.Append(command.ControlCode);
			if(command.ControlSelection >= 0) {
				sb.Append(command.ControlSelection);
			}
			if(command.ControlValue != float.MinValue) {
				sb.Append(" ");
				CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
				ci.NumberFormat.CurrencyDecimalSeparator = ".";
				sb.Append(string.Format("{0,7}", command.ControlValue.ToString("G7", ci)));
			}
			sb.Append(string.Format("{0}{1}", CheckSumDelimiter, CheckSum(sb.ToString())));
			return sb.ToString();
		}

		/*private void TrimN3N5N6ACK(ref string message) {
			if(((message.Contains("N3") || message.Contains("N5") || message.Contains("N6"))
				&& message.Contains("#?") && message.Contains(Terminator))|| message.Contains("R") || message.Contains("E") || message.Contains("P ")) {
					int pos = message.IndexOf(Terminator);
					if(pos > 0) {
						message = message.Substring(pos).TrimStart();
						_receiveBuffer.Clear();
						_receiveBuffer.Append(message.TrimStart('\r', '\n'));
					}
			}
		}*/

		private bool IsValidMessage(string buffer) {
			//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning, buffer.ToString()));
			if(buffer.Contains("#(ADD)") || (buffer.Contains("N") && buffer.Contains("#?"))) {
				return 2 <= buffer.Count((char x) => { return x.Equals(Terminator); });
			}
			return buffer.Contains(Terminator);
		}

		private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e) {
			//lock(_lockObject) {
				SerialPort sp = (SerialPort)sender;
				
				if(sp.IsOpen) {
					_receiveBuffer.Append(sp.ReadExisting());
				}

				string message = _receiveBuffer.ToString().TrimStart();
				//TrimN3N5N6ACK(ref message);
				while(IsValidMessage(message)) {
					int pos = message.IndexOf(Terminator);
					if(2 <= message.Count((char x) => { return x.Equals(Terminator); })) {
						pos = message.IndexOf(Terminator, pos + 1);
					}
					string currentMessage = message.Substring(0, pos).Trim().TrimStart('\r', '\n');
					_receiveBuffer.Clear();
					_receiveBuffer.Append(message.Substring(pos).TrimStart('\r', '\n'));

					if(currentMessage.StartsWith("!")) {
						int address = 0;
						try {
							int crcPos = currentMessage.IndexOf('|');
							if(crcPos > -1 && currentMessage.Length > 1) {
								currentMessage = currentMessage.Substring(1, pos);
								address = Int32.Parse(currentMessage);
							} else {
								currentMessage = currentMessage.Substring(1);
								address = Int32.Parse(currentMessage);
							}
						} catch(Exception) { }
						TriggerDeviceNotification(address);
					} else if(currentMessage.Length > 0) {
						//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning, "wait to lock @ receive"));
						lock(_lockObject) {
							//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning, "enter lock @ receive"));
							//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning, "Received set"));
							try { _communicationSync.Set(); } catch(Exception) { }
							try {
								ThreadedMessage threadedMessage = _requestOrder.Dequeue();
								//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning,
								//    string.Format("Sorból> ID: {0} üzenet: {1} válasz: {2}", threadedMessage.ID, threadedMessage.Message, currentMessage).Replace("\r", "[CR]")));
								threadedMessage.Message = currentMessage;

								TriggerTreadedMessageReceived(threadedMessage);
							} catch(Exception ex) {
								throw new Exception("Unexpected message arrived!", ex);
							}
						}
					}
					message = _receiveBuffer.ToString();
				}
				//if(!IsValidMessage(message) && message.Length > 0) {
				//    LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Notice,
				//        string.Format("part<<({0}, {1}, {2}, {3}, {4})L:{5} Data:[{6}]",
				//        sp.PortName, sp.BaudRate, sp.DataBits, sp.StopBits, sp.Parity, message.Length, message)));
				//}
			//}
		}
		
		public DPI520BaseAddressed(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, Handshake handshake) {
			_serialPort = new SerialPort(portName, baudRate, parity, dataBits, stopBits);
			_serialPort.Handshake = handshake;
			_serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
		}

		public DPI520BaseAddressed(string portName)
			: this(portName, 9600, Parity.Odd, 8, StopBits.One, Handshake.None) {
		}

		public virtual void Open() {
			try { _communicationSync.Set(); } catch(Exception) { }
			_serialPort.Open();
		}

		public virtual void Close() {
			_serialPort.Close();
		}

		public void WriteRawMessage(ThreadedMessage message) {
			//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning,
			//				string.Format("Sor hossza: {0} üzenet: {1}", _requestOrder.Count, message.Message).Replace("\r", "[CR]")));
			//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning, string.Format("({2})wait row: {0} --- {1}", _requestOrder.Count, message.Message, message.ID)));
			if(!_communicationSync.WaitOne(2100)) {
				throw new TimeoutException("Communication timed out.");
			}
			//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning, string.Format("wait succeeded: {0}", _requestOrder.Count)));
			//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning, "wait to lock @ send"));
			lock(_lockObject) {
				//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning, "enter lock @ send"));
				//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Warning,
				//                string.Format("Sorba< ID: {0} üzenet: {1} válasz: -", message.ID, message.Message).Replace("\r", "[CR]")));
				_requestOrder.Enqueue(message);
				_serialPort.Write(message.Message);

			}
		}

		public bool IsOpen {
			get {
				return _serialPort.IsOpen;
			}
		}
	}
}
