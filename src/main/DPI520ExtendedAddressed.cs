﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Globalization;
using FFW.NSDevices;

namespace FFW.Devices.NSDPI520 {
	public class DPI520ExtendedAddressed : DPI520BaseAddressed {

		protected const int FunctionID = 1;
		private AutoResetEvent _functionSync = new AutoResetEvent(false);
		private string _receivedMessage;
		protected object _lockObject = new object();

		protected virtual void OnTreadedMessageReceivedEvent(IRS232Device sender, ThreadedMessage message) {
			if(message.ID == FunctionID) {
				// Mivel nem érkezhet kérdés nélkül üzenet nem kerül Lock blokkba. Különben kéne.
				_receivedMessage = string.Copy(message.Message);
				_functionSync.Set();
				//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Error, "Function Set"));
			}
		}

		public DPI520ExtendedAddressed(string portName)
			: base(portName) {
			TreadedMessageReceivedEvent += new TreadedMessageReceived(OnTreadedMessageReceivedEvent);
		}


		public DPI520ExtendedAddressed(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, Handshake handshake)
			: base(portName, baudRate, parity, dataBits, stopBits, handshake) {
			TreadedMessageReceivedEvent += new TreadedMessageReceived(OnTreadedMessageReceivedEvent);
		}

		public DeviceInformation GetDeviceInformation(int address) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 5)));
			string message = string.Format("#?{0}", address.ToString("D2"));
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Getting device information from device '{0}' failed!", address.ToString("D2")));
			}
			int pos = _receivedMessage.IndexOf(Terminator);
			if(pos > 0) {
				_receivedMessage = _receivedMessage.Substring(pos).TrimStart();
			}
			string[] parts = _receivedMessage.Trim('{', '}').Split(' ');
			//if(parts.Length < 1) {
			//    throw new ArgumentException(string.Format("Invalid message returned from device {0}.", address.ToString("D2")));
			//}
			//parts = _receivedMessage.Split(' ');
			if(parts.Length < 5) {
				throw new ArgumentException(string.Format("Invalid message returned from device {0}.", address.ToString("D2")));
			}
			pos = parts[4].IndexOf('|');
			if(pos > -1) {
				parts[4] = parts[4].Substring(0, pos);
			}
			//parts[4].TrimEnd('}');
			return new DeviceInformation(parts[0], parts[1], parts[3], parts[4]);
		}

		public InterfaceInformation GetInterfaceInformation(int address) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 2)));
			string message = string.Format("#?{0}", address.ToString("D2"));
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Getting interface information from device '{0}' failed!", address.ToString("D2")));
			}
			_receivedMessage = _receivedMessage.Trim();
			string[] parts = _receivedMessage.Split('\r');
			if(parts.Length < 2) {
				throw new ArgumentException(string.Format("Invalid message returned from device."));
			}
			_receivedMessage = parts[1].Trim('{', '}'); ;
			ControlMode mode = (ControlMode)UInt16.Parse(_receivedMessage.Substring(4, 1));
			Scale scale = (Scale)UInt16.Parse(_receivedMessage.Substring(6, 1));
			DataSources source = (DataSources)UInt16.Parse(_receivedMessage.Substring(8, 1));
			bool controllerOpen = _receivedMessage.Substring(10, 1).Equals("1");
			Interrupts interrupt = (Interrupts)UInt16.Parse(_receivedMessage.Substring(12, 1));
			bool isolationValve = _receivedMessage.Substring(15, 1).Equals("1");

			return new InterfaceInformation(mode, scale, source, controllerOpen, interrupt, isolationValve);
		}

		public EventStatusInformation GetEventStatusInformation(int address) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 3)));
			string message = string.Format("#?{0}", address.ToString("D2"));
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Getting event status information from device '{0}' failed!", address.ToString("D2")));
			}
			return ParseEventStatusInformation(_receivedMessage);
		}

		protected EventStatusInformation ParseEventStatusInformation(string message) {
			int pos = message.IndexOf(Terminator);
			if(pos > 0) {
				message = message.Substring(pos).TrimStart();
			}
			message = message.Trim('{', '}');
			//if(parts.Length < 1 || parts[0].Contains("#?00")) {
			//    throw new ArgumentException(string.Format("Invalid message returned from device."));
			//}
			//message = parts[parts.Length - 1].Trim('{', '}');
			ushort inRange = UInt16.Parse(message.Substring(0, 1));
			ushort error = 0;
			int errorPos = message.IndexOf('@');
			if(errorPos > -1) {

				error = UInt16.Parse(message.Substring(errorPos + 1, 2), NumberStyles.HexNumber);
			}
			return new EventStatusInformation(inRange, error);
		}

		public SetupInformation GetSetupInformation(int address) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 8)));
			string message = string.Format("#?{0}", address.ToString("D2"));
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Getting setup information from device '{0}' failed!", address.ToString("D2")));
			}
			CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
			ci.NumberFormat.CurrencyDecimalSeparator = ".";

			_receivedMessage = _receivedMessage.Trim();
			string[] parts = _receivedMessage.Split('\r');
			if(parts.Length < 2) {
				throw new ArgumentException(string.Format("Invalid message returned from device."));
			}
			_receivedMessage = parts[1].Trim().Trim('{', '}');
			ControlMode mode = (ControlMode)UInt16.Parse(_receivedMessage.Substring(4, 1));
			Scale scale = (Scale)UInt16.Parse(_receivedMessage.Substring(6, 1));
			DataSources source = (DataSources)UInt16.Parse(_receivedMessage.Substring(8, 1));
			bool controllerOpen = _receivedMessage.Substring(10, 1).Equals("1");// bool.Parse(_receivedMessage.Substring(10, 1));
			Interrupts interrupt = (Interrupts)UInt16.Parse(_receivedMessage.Substring(12, 1));

			ushort notationCode = UInt16.Parse(_receivedMessage.Substring(14, 1));
			uint waitTime = UInt32.Parse(_receivedMessage.Substring(16, 3));
			ushort errorStatus = UInt16.Parse(_receivedMessage.Substring(20, 1), NumberStyles.HexNumber);
			ushort terminatorCode = UInt16.Parse(_receivedMessage.Substring(22, 1));
			ushort rate = UInt16.Parse(_receivedMessage.Substring(24, 1));
			float variableRate = float.Parse(_receivedMessage.Substring(26, 7), NumberStyles.Any, ci);
			//TODO:!!!! Parsolni, vagy elhagyni!
			//ScaleUnits unit = (ScaleUnits)UInt16.Parse(_receivedMessage.Substring(34, 6));
			bool isTareOn = _receivedMessage.Substring(41, 1).Equals("1");// bool.Parse(_receivedMessage.Substring(41, 1));
			float tareValue = float.Parse(_receivedMessage.Substring(43, 7), NumberStyles.Any, ci);

			return new SetupInformation(mode, scale, source, controllerOpen, interrupt, notationCode,
				waitTime, errorStatus, terminatorCode, rate, variableRate, /*ScaleUnits.mbar,*/ isTareOn, tareValue);
		}

		public CurrentStatus GetCurrentStatus(int address) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 6)));
			string message = string.Format("#?{0}", address.ToString("D2"));
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Getting current status information from device '{0}' failed!", address.ToString("D2")));
			}
			return ParseCurrentStatus(_receivedMessage);
		}

		protected CurrentStatus ParseCurrentStatus(string message) {
			int pos = message.IndexOf(Terminator);
			if(pos > 0) {
				message = message.Substring(pos).TrimStart();
			} else {
				throw new ArgumentException(string.Format("Invalid message returned from device."));
			}
			//string[] parts = message.Split('\r');
			//if(parts.Length < 2) {
			//    throw new ArgumentException(string.Format("Invalid message returned from device."));
			//}
			message = message.Trim('{', '}');
			CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
			ci.NumberFormat.CurrencyDecimalSeparator = ".";
			float pressure = float.Parse(message.Substring(0, 7).Trim('.', ' '), NumberStyles.Any, ci);
			ControlMode mode = (ControlMode)UInt16.Parse(message.Substring(11, 1));
			Scale scale = (Scale)UInt16.Parse(message.Substring(13, 1));
			DataSources source = (DataSources)UInt16.Parse(message.Substring(15, 1));
			ScaleUnits unit = (ScaleUnits)UInt16.Parse(message.Substring(17, 2));
			ushort error = 0;
			int errorPos = message.IndexOf('@');
			if(errorPos > -1) {
				error = UInt16.Parse(message.Substring(errorPos + 1, 2), NumberStyles.HexNumber);
			}
			return new CurrentStatus(pressure, mode, scale, source, unit, error);
		}

		public virtual void SetPressure(float pressure, Scale scale, ushort wait = 20) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.S, (ushort)scale)));
			sb.Append(Delimiter);
			//sb.Append(ComposeMessage(new ControlCommand(ControlCommands.I, 3)));
			//sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.P, pressure, ControlParameterType.ControlValue)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.W, wait)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));

			if(!_functionSync.WaitOne(WaitPeriod)) {
				//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Error, "Function wait failed."));
				throw new TimeoutException(string.Format("Setting set point pressure to '{0}' failed!", pressure));
			}
		}

		public virtual void SetPressure(float pressure, ScaleUnits unit = ScaleUnits.mbar, ushort wait = 20) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.S, 3)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.U, (int)unit)));
			sb.Append(Delimiter);
			//sb.Append(ComposeMessage(new ControlCommand(ControlCommands.I, 3)));
			//sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.P, pressure, ControlParameterType.ControlValue)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.W, wait)));
			sb.Append(Delimiter);
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.C, 1)));
			sb.Append(Terminator);
			//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Error, "Function wait start."));
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				//LogManager.Instance.TriggerLogEvent(new LogEntry(LogEntryType.Error, "Function wait failed."));
				throw new TimeoutException(string.Format("Setting set point pressure to '{0}{1}' failed!", pressure, unit));
			}
		}

		public ControlMode GetControlMode(int address) {
			CurrentStatus status = GetCurrentStatus(address);
			return status.ControlMode;
		}

		public void SetControlMode(ControlMode controlMode) {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}",
				ComposeMessage(new ControlCommand(ControlCommands.R, (int)controlMode)), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Setting control mode to '{0}' failed!", controlMode));
			}
		}

		public Scale GetScale(int address) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 6)));
			string message = string.Format("#?{0}", address.ToString("D2"));
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			WriteRawMessage(new ThreadedMessage(FunctionID, sb.ToString()));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Getting the scale value from the device '{0}' failed!", address.ToString("D2")));
			}
			string mode = string.Empty;
			if(_receivedMessage.Length > 15) {
				return (Scale)UInt16.Parse(_receivedMessage.Substring(14, 1));
			} else {
				throw new Exception("Invalid message returned while trying to determinate the scale value!");
			}
		}

		public void SetScale(Scale scale) {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}",
				ComposeMessage(new ControlCommand(ControlCommands.S, (ushort)scale)), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Setting scale to '{0}' failed!", scale));
			}
		}

		public void OpenIsolationValve() {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}", ComposeMessage(new ControlCommand(ControlCommands.E, 1)), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Opening isolation valve failed!");
			}
		}

		public void CloseIsolationValve() {
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}", ComposeMessage(new ControlCommand(ControlCommands.E, 0)), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Closing isolation valve failed!");
			}
		}

		public int[] GetDevices() {
			string message = "#(ADD)";
			WriteRawMessage(new ThreadedMessage(FunctionID, (string.Format("{0}{1}{2}{3}", message, CheckSumDelimiter, CheckSum(message), Terminator))));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Failed to get device addresses!");
			}
			string[] parts = _receivedMessage.Split(Terminator);
			if(parts.Length < 2) {
				throw new ArgumentException("Invalid message returned from devices.");
			}
			parts = parts[1].Replace("{", "").Replace("}", " ").Trim().Split(' ');
			int[] addresses = new int[parts.Length];
			for(int i = 0; i < parts.Length; i++) {
				int pos = parts[i].IndexOf('|');
				if(pos > -1) {
					parts[i] = parts[i].Substring(0, pos);
				}
				addresses[i] = Int32.Parse(parts[i].Replace("ADD", ""));
			}
			return addresses;
		}

		public void UnlistenAll() {
			string message = "#(UNL)";
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}{2}{3}", message, CheckSumDelimiter, CheckSum(message), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Failed to set devices to unlisten state!");
			}
		}

		public void Untalk() {
			string message = "#(UNT)";
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}{2}{3}", message, CheckSumDelimiter, CheckSum(message), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Failed to set current talker device to untalk state!");
			}
		}

		public void AddListener(int deviceAddress) {
			string message = string.Format("#L{0}", deviceAddress.ToString("D2"));
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}{2}{3}", message, CheckSumDelimiter, CheckSum(message), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Failed to add device '{0}' to listen state!", deviceAddress.ToString("D2")));
			}
		}

		public void SetTalker(int deviceAddress) {
			string message = string.Format("#T{0}", deviceAddress.ToString("D2"));
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}{2}{3}", message, CheckSumDelimiter, CheckSum(message), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException(string.Format("Failed to add device '{0}' to talk state!", deviceAddress.ToString("D2")));
			}
		}

		public void ClearInterface() {
			string message = "#(IFC)";
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}{2}{3}", message, CheckSumDelimiter, CheckSum(message), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Failed to clear interface!");
			}
		}

		public void ClearDevice() {
			string message = "#(DCL)";
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}{2}{3}", message, CheckSumDelimiter, CheckSum(message), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Failed to clear device state!");
			}
			if(!_receivedMessage.StartsWith("#(DCL)")) {
				throw new TimeoutException("Failed to clear device state!");
			}
		}

		public void DialogFormat() {
			string message = "#(DIA)";
			WriteRawMessage(new ThreadedMessage(FunctionID, string.Format("{0}{1}{2}{3}", message, CheckSumDelimiter, CheckSum(message), Terminator)));
			if(!_functionSync.WaitOne(WaitPeriod)) {
				throw new TimeoutException("Failed to set devices to dialog mode!");
			}
		}
	}
}
