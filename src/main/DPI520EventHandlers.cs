﻿// SPDX-License-Identifier: LGPL-3.0-only

using FFW.NSDevices;

namespace FFW.Devices.NSDPI520 {
	public delegate void DeviceThreadedNotification(IRS232Device sender, int address);
	public delegate void TreadedMessageReceived(IRS232Device sender, ThreadedMessage message);

	public delegate void PressureInRange(IRS232Device sender, PressureArgs args);
	public delegate void PressureChanged(IRS232Device sender, PressureArgs args);
	public delegate void ErrorOccured(IRS232Device sender, ushort error);
}
