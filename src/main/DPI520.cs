﻿using System;
using System.Text;
using System.IO.Ports;
using System.Threading;
// SPDX-License-Identifier: LGPL-3.0-only

using FFW.NSDevices;

namespace FFW.Devices.NSDPI520 {

	public class DPI520 : DPI520Extended, IDisposable {

		public event PressureInRange PressureInRangeEvent;
		public event PressureChanged PressureChangedEvent;
		public event ErrorOccured ErrorOccuredEvent;

		private bool _isOpened;
		private const int LoopID = 0;
		private string _receivedLoopMessage;
		private bool inRangeTriggered = false;
		private float _currentPressure;
		private DateTime _latestWrongPressureRead;
		private float _setPointPressure;
		private object _statusLock = new object();
		private object _eventLock = new object();

		private AutoResetEvent _loopFunctionSync = new AutoResetEvent(false);
		private AutoResetEvent _loopFunctionHandledSync = new AutoResetEvent(true);
		private volatile bool _loop = false;
		protected Thread _listenerThread = null;
		

		#region Event triggers

		protected void TriggerPressureInRange(PressureArgs args) {
			if(PressureInRangeEvent != null) {
				PressureInRangeEvent(this, args);
			}
		}

		protected void TriggerPressureChanged(PressureArgs args) {
			if(PressureChangedEvent != null) {
				PressureChangedEvent(this, args);
			}
		}

		protected void TriggerErrorOccured(ushort error) {
			if(ErrorOccuredEvent != null) {
				ErrorOccuredEvent(this, error);
			}
		}

		#endregion

		private void StopThread() {
			_loop = false;
			if(_listenerThread == null) {
				return;
			}
			_listenerThread.Join();
			_listenerThread = null;
		}

		protected override void OnTreadedMessageReceivedEvent(IRS232Device sender, ThreadedMessage message) {
			base.OnTreadedMessageReceivedEvent(sender, message);
			if(message.ID == LoopID) {
				_loopFunctionHandledSync.WaitOne(5000);
				_receivedLoopMessage = string.Copy(message.Message);
				_loopFunctionSync.Set();
			}
		}

		protected void GetEventStatusInformation(int id) {
			StringBuilder sb = new StringBuilder();
			sb.Append(ComposeMessage(new ControlCommand(ControlCommands.N, 3)));
			string message = string.Empty;
			sb.Append(string.Format("{0}|{1}", message, CheckSum(message)));
			sb.Append(Terminator);
			WriteRawMessage(new ThreadedMessage(LoopID, sb.ToString()));
		}

		protected void GetCurrentStatus(int id) {
			string message = string.Format("{0}{1}", ComposeMessage(new ControlCommand(ControlCommands.N, 6)), Terminator);
			WriteRawMessage(new ThreadedMessage(LoopID, message));
		}

		protected void SendNewLine(int id) {
			WriteRawMessage(new ThreadedMessage(id, Terminator.ToString()));
		}

		protected void InformationRefresher(object o) {
			bool result;
			_latestWrongPressureRead = DateTime.Now;

			CurrentStatus currentStatus = null;
			while(_loop) {
				try {
					GetCurrentStatus(LoopID);

					result = _loopFunctionSync.WaitOne(WaitPeriod);
					currentStatus = ParseCurrentStatus(_receivedLoopMessage);

					if(currentStatus != null) {
						if(_currentPressure != currentStatus.Pressure) {
							_currentPressure = currentStatus.Pressure;
							TriggerPressureChanged(new PressureArgs(currentStatus.Pressure, currentStatus.Unit));
						}
						if(currentStatus.ErrorCode != 0) {
							TriggerErrorOccured(currentStatus.ErrorCode);
						}
					}
					//if(currentStatus.Pressure != _setPointPressure) {
					if(Math.Abs(currentStatus.Pressure - _setPointPressure) > 0.5) {
						_latestWrongPressureRead = DateTime.Now;
					} else if(!inRangeTriggered && DateTime.Now.Subtract(_latestWrongPressureRead).Ticks > (new TimeSpan(0, 0, 45)).Ticks) {
						TriggerPressureInRange(new PressureArgs(currentStatus.Pressure, currentStatus.Unit));
						inRangeTriggered = true;
					}
					if(!_loop) {
						break;
					}
					Thread.Sleep(1000);
					if(!_loop) {
						break;
					}
				} catch(Exception) {
					continue;
				} finally {
					_loopFunctionHandledSync.Set();
				}
			}
		}

		public DPI520(string portName) : base(portName) {
		}


		public DPI520(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, Handshake handshake)
			: base(portName, baudRate, parity, dataBits, stopBits, handshake) {
		}

		public float MinValue { get { return float.Parse(ConstantPropertiesResource.MinValue); } }
		public float MaxValue { get; private set; }
		public string Unit { get { return ConstantPropertiesResource.RangeUnit; } }

		public string SerialNumber { get; private set; }

		public void AttachListenerhToDevice() {
			if(_listenerThread != null) {
				throw new Exception("Allready listening to a device. Detach listerner first.");
			}
			_listenerThread = new Thread(InformationRefresher);
			_loop = true;
			_listenerThread.Start();
		}

		public void DetachListenerFromDevice() {
			StopThread();
		}

		public void Dispose() {
			Close();
		}

		~DPI520() {
			Close();
		}

		public override void Open() {
			base.Open();
			
			SetControlMode(ControlMode.Remote);

			var di = GetDeviceInformation();
			SerialNumber = di.SerialNumber;
			MaxValue = float.Parse(di.FullScale.Substring(0, 2));

			_isOpened = true;
			AttachListenerhToDevice();
		}

		public override void Close() {
			if(_isOpened) {
				try {
					DetachListenerFromDevice();
				} catch(Exception) { }
				try {
					SetControlMode(ControlMode.Local);
				} catch(Exception) { }
			}
			base.Close();
		}

		public override void SetPressure(float pressure, Scale scale, ushort wait = 20) {
			_latestWrongPressureRead = DateTime.Now;
			inRangeTriggered = false;
			_setPointPressure = pressure;
			base.SetPressure(pressure, scale, wait);
		}

		public override void SetPressure(float pressure, ScaleUnits unit = ScaleUnits.mbar, ushort wait = 20) {
			_latestWrongPressureRead = DateTime.Now;
			inRangeTriggered = false;
			_setPointPressure = pressure;
			base.SetPressure(pressure, unit, wait);
		}

		public float Pressure { get { return _currentPressure; } }
	}
}
